# PlaintextWriting

Ein Artikel, der das Schreiben u.a. wissenschaftlicher Texte mit Markdown und Pandoc erklärt. Dabei kommen auch LaTeX und eine Literaturverwaltung zum Einsatz, um wissenschaftliche Texte korrekt umzusetzen.