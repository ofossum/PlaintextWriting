pdf: 01\ -\ wissenschaftliche\ Artikel\ mit\ Markdown\ und\ Pandoc\ schreiben.adoc
	asciidoctor-pdf -a pdf-theme=default-sans 01\ -\ wissenschaftliche\ Artikel\ mit\ Markdown\ und\ Pandoc\ schreiben.adoc

pdf-gentium: 01\ -\ wissenschaftliche\ Artikel\ mit\ Markdown\ und\ Pandoc\ schreiben.adoc
	asciidoctor-pdf -a pdf-theme=juju-gentium 01\ -\ wissenschaftliche\ Artikel\ mit\ Markdown\ und\ Pandoc\ schreiben.adoc
