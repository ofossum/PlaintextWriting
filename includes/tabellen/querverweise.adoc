
[cols="1,1"]
|===

2+^.^|*Querverweise*

^.|*Marker setzen*
^.|*auf Marker verweisen*

a|
----
<1>
…  {#sec:Überschrift}
…  {#tbl:Tabelle}
…  {#fig:Abbildung}
…  {#eq:Formel}
…  {#lst:Code}
----
a|
----
<1>
…  @sec:Überschrift
…  @tbl:Tabelle
…  @fig:Abildung
…  @eq:Formel
…  @lst:Code
----


|===

<<1>> Das _Setzen_ und _Verweisen_ eines Referenzpunktes benötigt jeweils die Angabe, welcher _Art_ das Ziel dieser Referenz ist.
